import json
# import postgres_connector

# connector = postgres_connector("jdbc://localhost", "admin", "admin")

"""
Assuming we have the following table structure: 

create table cars(
    id int,
    make varchar(200),
    primary key(id)
);

create table car_services(
    id int,
    year varchar(4),
    mechanic_shop varchar(200),
    car_id int,
    primary key(id),
    foreign key(car_id) references cars(id)
);

create table car_radios(
    id int,
    brand varchar(10),
    antenna_frequency varchar(5),
    antenna_length_units int,
    antenna_length_units_of_measurement varchar(5),
    car_id int,
    primary key (id),
    foreign key(car_id) references cars(id)
);  
"""

f = open("car.json", "r")
data = json.load(f)

make = data.get("make")

# insert make in the cars table
sql = f"insert (make) cars values ({make});"
print(sql)
#result = connector.query(sql)
#car_id = result.id
car_id = 1

services = data.get("services")
for s in services:
    year = s.get("year")
    mechanic_shop = s.get("mechanicShop")
    # insert services in the car_services table
    sql = f"insert (year, mechanic_shop, car_id) into car_services values ('{year}', '{mechanic_shop}', {car_id});"
    print(sql)

radios = data.get("radios")
for r in radios:
    brand = r.get("brand")
    has_cd_player = r.get("hasCdPlayer")
    has_gps = r.get("hasGps")
    antenna = r.get("antenna")
    if antenna:
        antenna_frequency = antenna.get("frequency")
        antenna_length_units = antenna.get("length").get("units")
        antenna_length_units_of_measurement = antenna.get("length").get("unitsOfMeasurement")
    else:
        antenna_frequency = None
        antenna_length_units = None
        antenna_length_units_of_measurement = None

    # insert services in the car_radios table
    sql = f"insert (brand, has_cd_player, has_gps, antenna_frequency, antenna_length_units, antenna_length_units_of_measurement) into car_radios values ('{brand}', {has_cd_player}, {has_gps}, '{antenna_frequency}', {antenna_length_units}, '{antenna_length_units_of_measurement}');"
    print(sql)