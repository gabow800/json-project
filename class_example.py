class Car:
    def __init__(self, make, year):
        self.make = make
        self.year = year

    def __str__(self):
        return f"Make: {self.make}, year: + {self.year}"

c = Car("Nissan", "2015")
print(c)
print(c.__str__())

